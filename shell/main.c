#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

#define MAX_LINE 4096
#define MAX_ARGS 2048

char *aux(char *str, int l, int r)
{
  char *rta = malloc(r - l + 1);
  for (int i = l; i < r; i++)
  {
    rta[i - l] = str[i];
  }
  rta[r - l] = '\0';
  return rta;
}

char **extractCommand(char *input)
{
  char **list = malloc(sizeof(char *) * MAX_ARGS);
  int l = 0, r = 0, c = 0, i;
  for (i = 0; input[i] != '\0'; i++)
  {
    if (input[i] == ' ' || input[i] == '\n')
    {
      r = i;
      list[c] = aux(input, l, r);
      l = i + 1;
      c++;
    }
  }
  list[i++] = NULL;
  return list;
}

int main(int argc, char **argv)
{

  char input[MAX_LINE];
  char **args;
  int wstatus, i;
  printf(">> ");
  for (fgets(input, MAX_LINE, stdin); strcmp(input, "exit\n"); fgets(input, MAX_LINE, stdin))
  {
    args = extractCommand(input);
    pid_t pid = fork();
    if (pid < 0)
    {
      perror("Fork has faield\n");
    }
    else if (pid == 0)
    {
      execv(args[0], args);
    }
    wait(&wstatus);
    printf(">> ");
    for (i = 0; args[i] != NULL; i++)
    {
      free(args[i]);
    }
    free(args[i]);
    free(args);
    fflush(stdin);
  }
  return 0;
}